# AddressBook

Address Book assignment using Django.

# Prerequisite

This has only been tested on window.
Python v3.9.1
pip v21.0.1
Django v3.1.6

**pip packages**
Pillow v8.1.0
djangorestframework v3.12.2

# Setup

1) cd to directory
2) virtualenv .
3) .\Scripts\activate
4) pip install django
4) pip install Pillow
4) pip install djangorestframework
4) git clone -url-
5) cd addressbook/src
6) python manage.py makemigrations
7) python manage.py migrate
8) python manage.py runserver
9) python manage.py createsuperuser --if you need to access /admin console of django

# Available

127.0.0.1:8000 (default) should take you to the main page portal

127.0.0.1:8000/admin should take you to the admin console of django

127.0.0.1:8000/api should take you to the api base

# Api
Body of create and update: (POST)

`
{
    "first_name": "str",
    "last_name": "str",
    "email": "str--email",
    "phone": "str",
    "company_name": "str",
    "job_title": "str",
    "address": "str",
    "created_by": int
}
`

more info available at 127.0.0.1:8000/api