
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from apps.contacts.models import Contact

# Create your views here.

@login_required
def dashboard_view(request):
    contacts = Contact.objects.filter(created_by=request.user).order_by('-updated_at')[0:3]
    context={
        "contacts": contacts,
        "userprofile": request.user.userprofile
    }
    return render(request, 'core/dashboard.html', context)


