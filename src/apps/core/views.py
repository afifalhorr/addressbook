from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login

from apps.contacts.models import Contact
from apps.userprofile.models import Userprofile

# Create your views here.
def homepage_view(request):
    # contacts = Contact.objects.all()[0:3]
    # context={
    #     "contacts": contacts
    # }
    return render(request, 'core/homepage.html')

def signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user =  form.save()
            userprofile = Userprofile.objects.create(user=user)
            userprofile.save()
            login(request, user)

            return redirect('homepage')
    else:
        form = UserCreationForm()

    context  = {
        'form' : UserCreationForm()
    }
    return render(request, 'core/signup.html', context)

# def logout_view(request):
#     return render(request, 'core/logoutpage.html')
