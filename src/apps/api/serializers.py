from rest_framework import serializers
from apps.contacts.models import Contact

class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ['id', 'first_name', 'last_name', 'email', 'phone', 'company_name', 'job_title', 'address', 'created_by']