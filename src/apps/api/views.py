from django.shortcuts import render
from django.http import JsonResponse

from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import ContactSerializer

from apps.contacts.models import Contact

# Create your views here.

@api_view(['GET'])
def apiOverview(request):
    api_urls = {
        'List':'/contact-list/',
        'Detail':'/contact-detail/<int:id>',
        'Create':'/contact-create/',
        'Update':'/contact-update/<int:id>',
        'Delete':'/contact-delete/<int:id>',
    }
    return Response(api_urls)

@api_view(['GET'])
def contactList(request):
    contacts = Contact.objects.all()
    serializer =  ContactSerializer(contacts, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def contactDetail(request, id):
    contacts = Contact.objects.get(pk=id)
    serializer =  ContactSerializer(contacts, many=False)
    return Response(serializer.data)


@api_view(['POST'])
def contactCreate(request):
    serializer =  ContactSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['POST'])
def contactUpdate(request, id):
    contacts = Contact.objects.get(pk=id)
    serializer =  ContactSerializer(instance=contacts, data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def contactDelete(request, id):
    contacts = Contact.objects.get(pk=id)
    contacts.delete()

    return Response('Contact Deleted')