from django.urls import path
from .views import apiOverview, contactList, contactDetail, contactCreate, contactUpdate, contactDelete

urlpatterns = [
    path('', apiOverview, name="api-overview"),
    path('contact-list/', contactList, name="contact-list"),
    path('contact-detail/<int:id>', contactDetail, name="contact-detail"),
    path('contact-create/', contactCreate, name="contact-create"),
    path('contact-update/<int:id>', contactUpdate, name="contact-update"),
    path('contact-delete/<int:id>', contactDelete, name="contact-delete"),
]