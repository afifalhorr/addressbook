from django.http import HttpResponse
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_list_or_404, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist

from .forms import CreateContactForm, ImageForm
from .models import Contact

# Create your views here.
@login_required
def contact_detail_view(request, contact_id):
    contact = Contact.objects.get(pk=contact_id)
    context={
        "contact": contact
    }
    return render(request, 'contacts/contact_detail.html', context)

@login_required
def contact_create_view(request):
    if request.method == 'POST':
        form = CreateContactForm(request.POST)

        if form.is_valid():
            contact = form.save(commit=False)
            contact.created_by = request.user
            contact.save()
            # redirect to list all next
            return redirect('contact_list')
    else:
        form = CreateContactForm()

    context = {
        "form": form
    }
    return render(request, 'contacts/contact_create.html', context)

@login_required
def contact_edit_view(request, contact_id):
    contact = get_object_or_404(Contact, pk=contact_id, created_by=request.user)
    if request.method == 'POST':
        form = CreateContactForm(request.POST, instance=contact)

        if form.is_valid():
            contact = form.save(commit=False)
            contact.created_by = request.user
            contact.save()
            # redirect to list all next
            return redirect('contact_list')
    else:
        form = CreateContactForm(instance=contact)

    context = {
        "form": form,
        "contact": contact
    }
    return render(request, 'contacts/contact_edit.html', context)

@login_required
def contact_list_view(request):
    # contact_list = get_list_or_404(Contact.objects.all().order_by('-updated_at'), created_by=request.user)
    try:
        contact_list = Contact.objects.filter(created_by=request.user).order_by('first_name')
    except ObjectDoesNotExist:
        contact_list = []
    paginator = Paginator(contact_list, 5) # Show 25 contacts per page.

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'list.html', {'page_obj': page_obj})

@login_required
def contact_delete_view(request, contact_id):
    obj = get_object_or_404(Contact, id=contact_id)
    if request.method == "POST":
        obj.delete()
        return redirect('contact_list')
        
    context = {
        "object":obj
    }
    return render(request, "contacts/contact_delete.html", context)

@login_required
def contact_photo_view(request, contact_id):
    contact = get_object_or_404(Contact, pk=contact_id, created_by=request.user)
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES, instance=contact)

        if form.is_valid():
            form.save()
            return redirect('../')
    else:
        form = ImageForm(instance=contact)
    return render(request, 'contacts/contact_photo.html', {"form": form, "contact": contact})

def contact_search_view(request):
    return render(request, 'contacts/contact_search.html')
