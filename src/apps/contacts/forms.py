from django import forms
from .models import Contact

class CreateContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'email', 'phone', 'address', 'job_title', 'company_name']

class ImageForm(forms.ModelForm): 
    class Meta: 
        model = Contact 
        fields = ['photo'] 