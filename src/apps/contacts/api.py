import json

from django.db.models import Q
from django.http import JsonResponse

from .models import Contact

def api_search(request):
    contactlist = []
    data = json.loads(request.body)
    query = data['query']

    contacts = Contact.objects.filter(Q(first_name__icontains=query) | Q(last_name__icontains=query) | Q(email__icontains=query) | Q(phone__icontains=query) | Q(address__icontains=query) | Q(job_title__icontains=query) | Q(company_name__icontains=query))

    for contact in contacts:
        obj = {
            'id': contact.id,
            'first_name': contact.first_name,
            'last_name': contact.last_name,
            'email': contact.email,
            'phone': contact.phone,
            'address': contact.address,
            'job_title': contact.job_title,
            'company_name': contact.company_name,
            'url': '/contact/%s/' % contact.id
        }
        contactlist.append(obj)

    return JsonResponse({'contacts':contactlist})
