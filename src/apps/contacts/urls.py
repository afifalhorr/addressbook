from django.urls import path, include

from .views import contact_detail_view, contact_create_view, contact_list_view, contact_edit_view, contact_delete_view, contact_photo_view, contact_search_view
from .api import api_search

urlpatterns = [
    path('api/search/', api_search , name='api_search'),
    path('search/', contact_search_view , name='search'),
    path('<int:contact_id>/', contact_detail_view , name='contact_detail'),
    path('create/', contact_create_view , name='contact_create'),
    path('<int:contact_id>/edit/', contact_edit_view , name='contact_edit'),
    path('<int:contact_id>/delete/', contact_delete_view , name='contact_delete'),
    path('list/', contact_list_view , name='contact_list'),
    path('<int:contact_id>/image/', contact_photo_view, name = 'contact_image_upload'),
]